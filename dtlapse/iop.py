class IopData:
    def __init__(self, operation: str, data: dict):
        self.operation = operation
        self.modversion: int = data["modversion"]
        self.iop_order: int = data.get("iop_order", 0)

        if "smooth" not in data:
            raise ValueError
        if (
            isinstance(data["smooth"], str) 
            and data["smooth"].startswith("eval")
        ):
            self.smooth: list[bool] = eval(data["smooth"][5:])
        else:
            self.smooth: list[bool] = data["smooth"]
        
        if "cformat" not in data:
            raise ValueError
        if (
            isinstance(data["cformat"], str)
            and data["cformat"].startswith("eval")
        ):
            self.cformat: str = eval(data["cformat"][5:])
        else:
            self.cformat: str = data["cformat"]


class Iop:
    def __init__(self, data: dict):
        if "operation" not in data or "iopdata" not in data:
            raise ValueError
        self.operation: str = data["operation"]
        self.iopdata: dict[int, IopData] = {
            d["modversion"]:
            IopData(operation=self.operation, data=d) for d in data["iopdata"]
        }
        self.help: str = data.get("help", "")
        self.license: str = data.get("license", "")
        self.copyright: str = data.get("copyright", "")